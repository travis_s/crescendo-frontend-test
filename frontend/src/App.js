import React, { useState, useEffect } from 'react';
import './App.css';

/* import { CardList } from './components/cardList/cardList'; */

const App = () => {
  const [recipes, setRecipes] = useState([]);
 // const [searchRecipes, setSearchRecipes] = useState(''); will use to create search function


 useEffect(() => {
  fetch(
    "http://localhost:3001/recipes"
  )
    .then((response) => response.json())
    .then((json) => setRecipes(Object.keys(json.data))); //In my original application, Object.keys needed to be used due to way data is stored. Object.map should work for this instance, but throws error.
}, []);



  return(
    <div>
      <h1>The Cook Book</h1>
      {recipes.map(recipe => (
        <h1 key={recipe.id}>{recipe.title}</h1>
      ))}
    </div>
    {/* <CardList recipes={recipes} /> */}
  );
};

export default App; 