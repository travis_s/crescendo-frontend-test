import React from 'react';

import './card.css';

export const Card = props => (

  <div className='card-container'>
      {/* <img src={`http://localhost:3001/${props.recipe.image.full}`} /> */}
      <h2>{props.recipe.title}</h2>
      <p>{props.recipe.description}</p>
  </div> 
);