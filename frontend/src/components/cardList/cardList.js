import React from 'react';

import './cardList.css';

import { Card } from '../card/card';

export const CardList = props => {
  console.log(props)
  return (
    <div className='card-list'>
    {props.recipes.map((recipe) => (
      <Card key={recipe.id} recipe={recipe} />
        ))}
    </div> 
  )
}